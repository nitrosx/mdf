function res = delete(obj,query)
    % function res = obj.delete(query)
    %
    % alias function for remove
    % please refere to remove function for help

    res = obj.remove(query);

end %function
